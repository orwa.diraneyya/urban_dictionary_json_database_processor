import os
import csv
import json
import jsontable as jsontable
import sys
from wordfreq import zipf_frequency

if __name__ == "__main__":
        if len(sys.argv) < 2:
                print("Not enough arguments")
                exit(-1)
                
        database = []

        # Upper level loop will go here
        part_identifier = sys.argv[1]

        file_json = open(part_identifier)
        if not file_json:
                print("JSON file was not found")
                exit(-2)

        print("STEP 1: JSON file found")
        
        filter_file = open("swear_words.txt", 'r');
        if not filter_file:
            print("Curse words text file not found")
            exit(-3)
                
        filter_array = filter_file.readlines()
    
        filter_dict = {}
        for curse_word in filter_array:
            filter_dict[curse_word[:-1]] = True
            
        filter_file.close()
    
        print("STEP 2: CURSE WORD file found")
        
        filtered_curse_words = 0
        filtered_common_words = 0
        common_words = []
        while True:
                line = file_json.readline()
                if not line:
                        break
                database.append(json.loads(line))
                if database[-1]["lowercase_word"] in filter_dict:
                    del database[-1]
                    filtered_curse_words += 1

        file_json.close

        print("STEP 3: JSON lines read successfully into an OBJECT ({0} curse words filtered out)".format(filtered_curse_words))

        with open(part_identifier + ".csv", 'w', newline='') as file_output:
                file_output.write('word;defid;popularity;activity;frequency;definition;\n')
                
                for definition_element in database:
                        temp_word = definition_element["lowercase_word"]
                        temp_word_frequency = zipf_frequency(definition_element["lowercase_word"], 'en')
                        
                        if temp_word_frequency > 3.5:
                            filtered_common_words += 1
                            common_words.append(temp_word)
                            continue
                            
                        temp_upvotes = int(definition_element["thumbs_up"])
                        temp_downvotes = int(definition_element["thumbs_down"])
                        file_output.write('"{0}";"{1}";"{2}";"{3}";"{4}";"{5}";\n'.format(
                                definition_element["lowercase_word"].replace('"', ''),
                                definition_element["defid"],
                                temp_upvotes - temp_downvotes,
                                temp_upvotes + temp_downvotes,
                                str(zipf_frequency(definition_element["lowercase_word"], 'en')).replace('.', ','),
                                definition_element["definition"].replace('"', "'")))

        print("STEP 4: OBJECT reduced and written out as '{0}.csv' successfully ({1} common words dropped)".format(part_identifier, filtered_common_words))
        
        common_words_file = open(part_identifier + '.common.txt', 'w')
        common_words_file.write('\n'.join(common_words))
        common_words_file.close()
        
        print("STEP 5: common words dropped were written out to a text file")
        
        exit(0)

        # The code below is discarded: TOO SLOW
        
        paths = [{"$.lowercase_word":"word"},
                 {"$.defid":"defid"},
                 {"$.thumbs_up":"thumbs_up"},
                 {"$.thumbs_down":"thumbs_down"}]
        
        converter = jsontable.converter()
        converter.set_paths(paths)
        database2 = converter.convert_json(database)
        del database

        print("STEP 3: OBJECT reduced")
        
        with open(part_identifier + ".csv", 'w', newline='') as file_output:
                csv_writer = csv.writer(file_output, delimiter=';')
                csv_writer.writerows(database2)

        print("STEP 4: reduced OBJECT written out as '{0}.csv' successfully".format(part_identifier))
                  
        exit(0)
