# urban_dictionary_processor

Some code I wrote to process the scraped JSON database published in this reddit post: https://www.reddit.com/r/datasets/comments/63spoc/19gb_of_urban_dictionary_definitions_1999_may_2016/

## Goal

To overcome gaps in western cultureal references for people who grew up elsewhere (e.g. in the Middle East). Having an Excel sheet with the most popular Urban Dictionary definitions can be a nice before-bed reading and a good basis for games and activities revolving around culture and western references.

This work took around 6 hours and was engaging and fun. I will be happy to see others improving on this or supplying more recent scraped versions of the Urban Dictionary database.

## Process

After downloading and uncompressing the JSON file in the reddit post I split it into 5 parts (`xaa`, `xab`, `xac`, `xad`, and `xae`) using this bash command:

```
  split --verbose -l550000 words.js
```

From this point on all I needed to do was to write a script that could process these JSON lines into CSV lines while:
- Only focusing on data I considered relevant, allowing me to sort the entries by popularity/relevance to modern culture
- Exclude obvious definitions

For the second point above I used a combination of a profanity word list (i.e. a censor word text file) and a word fequency library. Not perfect but resulted in an okay looking result.

To convert the split files into csv outputs you need to run the following commands:

```
  python ./reduce_json_to_csv.py xaa
  python ./reduce_json_to_csv.py xab
  python ./reduce_json_to_csv.py xac
  python ./reduce_json_to_csv.py xad
  python ./reduce_json_to_csv.py xae
``` 